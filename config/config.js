//本文件用于配置项目所使用的数据库的信息
module.exports = {
  DBHOST: "127.0.0.1",//主机
  DBPORT: "27017",//端口
  DBNAME: "accountProject",//数据库名称
}