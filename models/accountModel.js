//导包
const mongoose = require("mongoose");
//创建文档结构对象 声明各属性都是什么数据类型的(约束本集合中文档的属性及属性值的类型)
let AccountSchema = new mongoose.Schema({
  title: {//事项
    type: String,
    required: true,
  }, 
  time: {//时间
    type: Date, 
  },
  type: {//类型，支出还是收入
    type: Number,
    default: -1,
  },
  account: {//金额
    type: Number,
    required: true,
  },
  remarks: {//备注
    type: String,
  }
});
//创建模型对象 对文档操作的封装对象(增删改查均可) 第一个参数要求集合名称 第二个参数要求文档结构对象
let AccountModel = new mongoose.model("accounts", AccountSchema);

module.exports = AccountModel;