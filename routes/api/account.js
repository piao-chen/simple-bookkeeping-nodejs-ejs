//存放与账单相关的接口
const express = require('express');
const router = express.Router();
const moment = require("moment");
//同时格式化日期对象
const AccountModel = require('../../models/accountModel');
//导入自己封装的中间件
const checkTokenMidware = require("../../midwares/checkToken");

// 记账本的列表
router.get('/account', checkTokenMidware, function(req, res, next) {
  console.log(req.user);
  //配上中间件
  //获取所有账单信息
  AccountModel.find().sort({time: -1}).then((data)=>{
    //响应成功
    res.json({
      //响应状态码，使用0000来表示响应成功，很多都是这么来表示的，用一串0表示成功，非零表示失败。
      code: "0000",
      //响应信息
      msg: "success",
      //响应数据
      data: data
    });
  }).catch((err)=>{
    //响应失败
    res.json({
      code: "1001",
      msg: "error",
      data: null,
    });
  });
});

//新增记录
router.post("/account", checkTokenMidware, (req, res, next)=>{
  //做表单验证：根据不同的错误信息返回不同的错误编码，方便前端做修改
  //差不多就下面这样子
  // var postObj = req.body;
  // if(typeof postObj.title !== String){
  //   res.json({code: "1000", msg: "title is not string", data: null});
  //   return;
  // }
  //提交来的数据在req.body里面
  //直接提交过来的数据类型有的是不对的 因此要观察数据类型然后进行数据转换后再放入数据库
  //插入数据库
  AccountModel.create({
    ...req.body,//展开运算符合并对象
    //修改time属性的值
    time: moment(req.body.time).toDate(),
  }).then((data)=>{//记得使用promise形式的回调
    res.json({
      code: "0000",
      msg: "success",
      data: data
    });
  }).catch((err)=>{
    res.json({
      code: "1001",
      msg: "error",
      data: null,
    });
  });
});

//删除记录
router.delete("/account/:id", checkTokenMidware, (req, res)=>{
  //获取paramas的id参数
  let id = req.params.id;
  //根据id删除id中对应的对象
  AccountModel.deleteOne({_id: id}).then((data)=>{
    res.json({
      code: "0000",
      msg: "success",
      data: null
    });
  }).catch((err)=>{
    res.json({
      code: "1001",
      msg: "error",
      data: null
    });
  });
});

//获取单个账单信息的接口
router.get("/account/:id", checkTokenMidware, (req, res)=>{
  let {id} = req.params;
  AccountModel.findById(id).then((data)=>{
    res.json({
      code: "0000",
      msg: "success",
      data: data
    });
  }).catch((err)=>{
    res.json({
      code: "1000",
      msg: "error",
      data: null
    });
  });
});

//更新单个的账单信息
router.patch("/account/:id", checkTokenMidware, (req, res)=>{
  let {id} = req.params;
  AccountModel.updateOne({_id: id}, req.body).then((data)=>{
    //根据RESTful接口规则，更新需要返回的数据为更新后的那条数据
    //为了完成这件事，我们再次查询一次数据库
    AccountModel.findById(id).then((data)=>{
      res.json({
            code: "0000",
            msg: "success",
            data: data
          });
    }).catch((err)=>{
      res.json({
        code: "1000",
        msg: "error",
        data: null
      });
    })
  }).catch((err)=>{
    res.json({
      code: "1000",
      msg: "error",
      data: null
    });
  });
});

module.exports = router;
