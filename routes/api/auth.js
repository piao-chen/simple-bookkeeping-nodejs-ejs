//网页返回路由规则。
var express = require('express');
var router = express.Router();
//拿到user的数据库模型
const UserModel = require("../../models/userModel");
//用于加密
const md5 = require("md5");
//引入jwt
const jwt = require("jsonwebtoken");
//导入自己写的密钥
const serect = require("../../config/serectCode");

//进行登录
router.post("/login", (req, res)=>{
  //查询数据库，检测有无匹配记录
  //获取用户名和密码
  let {username, password} = req.body;
  UserModel.findOne({username: username, password: md5(password)}).then((data)=>{
    //正确匹配上，data是数据对象，匹配不上，data就是null
    if(data){
      let token = jwt.sign({username: data.username, _id: data._id}, serect, {expiresIn: 60*60*24*7});
      //对上之后返回token
      res.json({
        code: "0000",
        msg: "success",
        data: token
      })
    }else{
      //这里要加return防止两个end冲突
      return res.json({
        code: "2002",
        msg: "wrong username or password",
        data: null
      });
    }
  }).catch((err)=>{
    res.json({
      code: "2001",
      msg: "database error",
      data: null
    });
  });
});

//进行退出登录
router.post("/logout", (req, res)=>{
  //只需要销毁session即可达到退出登录的目的
  req.session.destroy(()=>{
    res.render("success", {msg: "退出登录成功！", url: "/login"});
    isNot = false;
  });

});

module.exports = router;
