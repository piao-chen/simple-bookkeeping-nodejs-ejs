//网页返回路由规则。
var express = require('express');
var router = express.Router();
//拿到user的数据库模型
const UserModel = require("../../models/userModel");
//用于加密
const md5 = require("md5");

//注册页面
router.get("/register", (req, res)=>{
  res.render("auth/register");
});

//进行注册
router.post("/register", (req, res)=>{
  //获取请求体数据
  //插入数据库，对密码进行加密
  UserModel.create({...req.body, password: md5(req.body.password)}).then((data)=>{
    res.render("success", {msg: "注册成功！", url: "/login"});
  }).catch((err)=>{
    res.status(500).send("注册失败！");
  });
});

//登录页面
router.get("/login", (req, res)=>{
  res.render("auth/login");
});

//进行登录
router.post("/login", (req, res)=>{
  //查询数据库，检测有无匹配记录
  //获取用户名和密码
  let {username, password} = req.body;
  UserModel.findOne({username: username, password: md5(password)}).then((data)=>{
    //正确匹配上，data是数据对象，匹配不上，data就是null
    if(data){
      //登录成功后写入session
      req.session.username = data.username;
      req.session._id = data._id;
      //render之后的代码不执行了！！记得别往那后面放！！！
      res.render("success", {msg: "登录成功", url: "/"});
    }else{
      res.send("账号或密码错误！");
    }
  }).catch((err)=>{
    res.status(500).send("登录失败");
  });
});

//进行退出登录
router.post("/logout", (req, res)=>{
  //只需要销毁session即可达到退出登录的目的
  req.session.destroy(()=>{
    res.render("success", {msg: "退出登录成功！", url: "/login"});
    isNot = false;
  });

});

module.exports = router;
