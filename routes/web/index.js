//网页返回路由规则。
var express = require('express');
var router = express.Router();
//导包moment 处理字符串转为日期对象
const moment = require("moment");
//同时格式化日期对象
const AccountModel = require('../../models/accountModel');
//导入自己写的判断登没登录的中间件
const checkLoginMidware = require("../../midwares/checkLogin");

//挂载首页
router.get('/', function(req, res, next) {
  res.render("index");
});

// 记账本的列表
router.get('/account', checkLoginMidware, function(req, res, next) {
  //获取所有账单信息
  AccountModel.find().sort({time: -1}).then((data)=>{
    //使用ejs语法实现传递到模板当中数据 将moment交给ejs用于格式化时间
    res.render("list", {list: data, moment});
  }).catch((err)=>{
    res.status(500).send("读取失败。");
  });
});

//挂载创建账本页面
router.get('/account/create', checkLoginMidware, function(req, res, next) {
  res.render("create");
});

//新增记录
router.post("/account", checkLoginMidware, (req, res, next)=>{
  //提交来的数据在req.body里面
  //直接提交过来的数据类型有的是不对的 因此要观察数据类型然后进行数据转换后再放入数据库
  //插入数据库
  AccountModel.create({
    ...req.body,//展开运算符合并对象
    //修改time属性的值
    time: moment(req.body.time).toDate(),
  }).then((data)=>{//记得使用promise形式的回调
    //成功就渲染成功的页面
    res.render("success", {msg: "添加成功！！", url: "/account"});//ejs语法
  }).catch((err)=>{
    console.log(err);
  })
});

//删除记录
router.get("/account/:id", checkLoginMidware, (req, res)=>{
  //获取paramas的id参数
  let id = req.params.id;
  //根据id删除id中对应的对象
  AccountModel.deleteOne({_id: id}).then((data)=>{
    res.render("success", {msg: "删除成功！", url: "/account"});
  }).catch((err)=>{
    res.status(500).send("删除失败");
  });
  
});

module.exports = router;
